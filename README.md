# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


Those are quick notes that I adapted from :
http://www.greenhills.co.uk/2013/03/24/cloning-vms-with-kvm.html

also there are scripts attached
Tested on customized RH 7.2 workstation 

but should work on any linux distro where libvirt is supported.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

This will cover short term vm cloning.

# 1. Network preparation #
```
python generate-ips.py  > ips.txt
```
# 2. Create RO base image: #
```
sudo qemu-img convert -O qcow2 /dev/vg_vms/ubuntu-base-vm /var/lib/libvirt/images/ubuntu-base-vm-readonly.qcow2
sudo chmod u-w /var/lib/libvirt/images/ubuntu-base-vm-readonly.qcow2
```
# 3. Procedure for creating new VM: #
a) Create  new differential qcow file based on backing (base image)
```
VM=vm114
sudo qemu-img create -f qcow2 -b /var/lib/libvirt/images/ubuntu-base-vm-readonly.qcow2 /var/lib/libvirt/images/$VM.qcow2
```
b)dump settings from template vm and change mac to match ip
```
 mkdir -p tmp
 virsh dumpxml ubuntu-base-vm > tmp/ubuntu-base-vm.xml
 mac=`egrep "^$VM"'\s' ips.txt | awk '{print $3}'`
 python ./modify-domain.py \
 --name $VM \
 --new-uuid \
 --device-path=/var/lib/libvirt/images/$VM.qcow2 \
 --mac-address $mac \
 < tmp/ubuntu-base-vm.xml > tmp/$VM.xml
 virsh define tmp/$VM.xml
 virsh start $VM

```
This will create a new vm that has to be customized ( Ip, hostname change) - TO DO : automate

c)
when we are satisfied with it we can create a snapshot :
```
virsh destroy $VM
sudo qemu-img create -f qcow2 -b /var/lib/libvirt/images/$VM.qcow2 /var/lib/libvirt/images/$VM-start.qcow2
virsh dumpxml $VM > tmp/$VM.xml
python ./modify-domain.py \
--device-path=/var/lib/libvirt/images/$VM-start.qcow2 \
< tmp/$VM.xml > tmp/$VM-start.xml
virsh define tmp/$VM-start.xml

virsh start $VM
```
d)
we can use 
```
resetvm.sh <vm-name> 
```
to revert to that snapshot ( useful for testing changes by ansible , install scripts etc)



### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact