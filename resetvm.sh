#!/bin/bash
VM=$1
virsh destroy $VM
sudo qemu-img create -f qcow2 -b /var/lib/libvirt/images/$VM.qcow2 /var/lib/libvirt/images/$VM-start.qcow2
virsh dumpxml $VM > tmp/$VM.xml
python ./modify-domain.py \
    --device-path=/var/lib/libvirt/images/$VM-start.qcow2 \
    < tmp/$VM.xml > tmp/$VM-start.xml
virsh define tmp/$VM-start.xml
virsh start $VM
